import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';

import React from 'react'
import {render} from 'react-dom'
import {App} from './app'

export default function app (bootstrapConfig) {
  const {settings, element} = bootstrapConfig;
  if (element === undefined) {
    throw new Error('Document element is required for this bootstrap');
  }
  render(<App {...(settings || {})}/>, element);
}
