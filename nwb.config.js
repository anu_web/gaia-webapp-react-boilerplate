module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'gaia_webapp_boilerplate_react',
    }
  },
  webpack: {
    html: {
      template: 'demo/src/index.html'
    }
  },
  babel: {
    runtime: true
  }
};
